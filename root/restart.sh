#!/bin/sh
/etc/init.d/exim4 stop
/etc/init.d/exim4 start

/etc/init.d/bind9 stop
/etc/init.d/bind9 start

/etc/init.d/nginx stop
/etc/init.d/nginx start

/etc/init.d/dovecot stop
/etc/init.d/dovecot start

/etc/init.d/monit stop
/etc/init.d/monit start
