#!/bin/sh -e

CUR_DIR="$( cd "$( dirname "$0" )" && pwd )"
DEFAULT_ROOT_MNT=/data

fail () {
    echo $1
    exit 1
}

/usr/bin/apt-get install -y vim-nox nginx bind9 encfs php5-cgi pwgen monit \
    ntpdate exim4-config exim4-daemon-light dovecot-imapd dovecot-pop3d

echo -n "Enter root folder path [${DEFAULT_ROOT_MNT}]: "
read ROOT_MNT
test -z "$ROOT_MNT" && ROOT_MNT=$DEFAULT_ROOT_MNT
test -d "$ROOT_MNT" || fail "Root folder $ROOT_MNT does not exist."

test -d /var/www && mv /var/www /var/www.bak
ln -s "$ROOT_MNT/var/www" /var

echo 'Setting up vim'
/bin/cp -r "$CUR_DIR"/root/* "$ROOT_MNT"
/bin/ln -sf "$ROOT_MNT/etc/vim/vimrc.local" /etc/vim

echo 'Setting up php-fastcgi'
/bin/ln -sf "$ROOT_MNT/etc/init.d/php-fastcgi" /etc/init.d
/bin/chmod 755 /etc/init.d/php-fastcgi
/usr/sbin/update-rc.d php-fastcgi defaults

/bin/ln -sf "$ROOT_MNT/etc/default/php-fastcgi" /etc/default

echo 'Setting up bind'
test -d /etc/bind && mv /etc/bind /etc/bind.bak
mkdir -p "$ROOT_MNT/etc/bind/domains"
ln -sf "$ROOT_MNT/etc/bind" /etc
chown root:bind "$ROOT_MNT"/etc/bind/named.conf*
chmod 640 "$ROOT_MNT"/etc/bind/named.conf*
test -f /etc/apparmor.d/usr.sbin.named && sed -i "$ i \ \ $ROOT_MNT/etc/bind/** rw,\n\ \ $ROOT_MNT/etc/bind/ rw," \
    /etc/apparmor.d/usr.sbin.named
test -f /etc/init.d/apparmor && /etc/init.d/apparmor reload
rndc-confgen -a

echo 'Setting up nginx'
test -d /etc/nginx && mv /etc/nginx /etc/nginx.bak
ln -sf "$ROOT_MNT/etc/nginx" /etc
rm -f /etc/logrotate.d/nginx
test -d /var/log/nginx && mv /var/log/nginx /var/log/nginx.bak
mkdir -p "$ROOT_MNT/var/log/nginx" "$ROOT_MNT/var/log/nginx-archive" "$ROOT_MNT/etc/nginx/conf.d"
ln -s "$ROOT_MNT/var/log/nginx" /var/log
test -d /var/log/nginx-archive && mv /var/log/nginx-archive /var/log/nginx-archive.bak
ln -s "$ROOT_MNT/var/log/nginx-archive" /var/log

echo 'Setting up monit'
test -d /etc/monit && mv /etc/monit /etc/monit.bak
ln -s "$ROOT_MNT/etc/monit" /etc
ln -sf "$ROOT_MNT/etc/default/monit" /etc/default

echo 'Setting up exim'
test -d /etc/exim4 && mv /etc/exim4 /etc/exim4.bak
ln -sf "$ROOT_MNT/etc/exim4" /etc
test -d /var/lib/mail && mv /var/lib/mail /var/lib/mail.bak
mkdir -p "$ROOT_MNT/var/lib/mail"
ln -s "$ROOT_MNT/var/lib/mail" /var/lib
chown Debian-exim:Debian-exim "$ROOT_MNT/var/lib/mail"
test -d /var/log/exim4 && mv /var/log/exim4 /var/log/exim4.bak
mkdir -p "$ROOT_MNT/var/log/exim4"
ln -s "$ROOT_MNT/var/log/exim4" /var/log
chown Debian-exim:adm "$ROOT_MNT/var/log/exim4"
chmod 2750 "$ROOT_MNT/var/log/exim4"
mkdir -p "$ROOT_MNT/etc/exim4/valiases" "$ROOT_MNT/etc/exim4/vdomains"
chown Debian-exim:Debian-exim "$ROOT_MNT/etc/exim4/valiases" "$ROOT_MNT/etc/exim4/vdomains"
mkdir -p "$ROOT_MNT/var/spool/exim4/db" "$ROOT_MNT/var/spool/exim4/input" "$ROOT_MNT/var/spool/exim4/msglog"
chmod -R 0750 "$ROOT_MNT/var/spool/exim4"
chown -R Debian-exim:Debian-exim "$ROOT_MNT/var/spool/exim4"
test -d /var/spool/exim4 && mv /var/spool/exim4 /var/spool/exim4.bak
ln -s "$ROOT_MNT/var/spool/exim4" /var/spool

echo 'Generating key/certificate'
"$CUR_DIR"/exim-gencert --force

echo 'Setting up dovecot'
test -d /etc/dovecot && mv /etc/dovecot /etc/dovecot.bak
ln -sf "$ROOT_MNT/etc/dovecot" /etc
EXIM_UID=`id -u Debian-exim`
EXIM_GID=`id -g Debian-exim`
sed -i "s/first_valid_uid = .*/first_valid_uid = $EXIM_UID/" /etc/dovecot/dovecot.conf
sed -i "s/first_valid_gid = .*/first_valid_gid = $EXIM_GID/" /etc/dovecot/dovecot.conf
sed -i "s/args = uid=[0-9]* gid=[0-9]* \(.*\)/args = uid=$EXIM_UID gid=$EXIM_GID \1/" /etc/dovecot/dovecot.conf

echo 'Installing cron tasks'
sed -i "s#_ROOT_#${ROOT_MNT}#" "$ROOT_MNT/etc/cron.d/nginx"
for i in "$ROOT_MNT"/etc/cron.d/*; do
    ln -sf $i /etc/cron.d
done


# Restart all services
